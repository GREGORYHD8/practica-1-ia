# Practica-1-IA

Primera practica Inteligencia Artificial de Gregory Heredia = 19-SISN-2-041  



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto1_Pilas1
{
    class Program
    {
        class clsPila
        {
            private int Max;
            private int Top;
            int[] pila;


            public clsPila(int tamaño)
            {
                Max = tamaño;
                Top = 1;
                pila = new int[tamaño];
            }

            private bool EstaLlena()
            {
                if (Top == Max) return true;
                return false;
            }
            private bool EstaVacia()
            {
                if (Top == 1) return true;
                return false;
            }

            public bool Push(int dato)
            {
                if (!EstaLlena())
                {
                    pila[Top] = dato;
                    Top++;
                    return true;
                }
                else return false;
            }

            public bool Pop()
            {
                if (!EstaVacia())
                {
                    Top--;
                    return true;
                }
                else return false;
            }

            public string Mostrar()
            {
                string resultado = "";
                if (!EstaVacia())
                {
                    for (int i = 1; i < Top; i++)
                    {
                        resultado += "Posicion: " + i + " Dato: " + pila[i] + "\n";
                    }
                    resultado += "Top: " + Top;
                    resultado += "Maximo: " + Max;
                    return resultado;
                }
                else return resultado = "La Pila Esta Vacia, Ingrese Datos Utilizando La Opcion 1.";
            }

            public void Vaciar()
            {
                Top = 0;
            }

            static void Main(string[] args)
            {
                clsPila pila = new clsPila(10);
                int respuesta;
                do
                {
                    Console.Clear();
                    Console.WriteLine();
                    Console.WriteLine("                                  --------Ejemplo de pila--------");
                    Console.WriteLine();
                    Console.WriteLine("                    --------A Continuacion Escoja Que Desea Realizar--------");
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.Write(" #1.- Insertar Dato.       ");
                    Console.Write("#2.- Eliminar Dato.       ");
                    Console.Write("#3.- Mostrar Datos.       ");
                    Console.WriteLine("#4.- Vaciar Datos.       ");
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.Write("Ingrese Un Valor Del 1 Al 4: ");
                    int opcion = int.Parse(Console.ReadLine());
                    Console.WriteLine();
                    switch (opcion)
                    {
                        case 1:
                            Console.Write("Ingrese El Dato: ");
                            if (pila.Push(int.Parse(Console.ReadLine()))) Console.WriteLine("Dato Insertado Con Exito"); 
                            else Console.WriteLine("Ocurrio Un Error");
                            break;
                        case 2:
                            if (pila.Pop()) Console.WriteLine("Ultimo Dato Eliminado Con Exito.");
                            else Console.WriteLine("Ocurrio Un Error, Inserte Primero Un Dato, Usando La Opcion 1.");
                            break;
                        case 3:
                            Console.WriteLine(pila.Mostrar());
                            break;
                        case 4:
                            Console.WriteLine("Datos Limpiados Correctamente");
                            Console.WriteLine();
                            pila.Vaciar();
                            break;
                        default:
                            Console.WriteLine("No Existe Dicha Opcion, Inserte Algo Valido Del 1 Al 4.");
                            Console.WriteLine();
                            Console.WriteLine();
                            break;
                    }
                    Console.WriteLine("¿Desea Realizar Otra Opcion ?");
                    Console.WriteLine();
                    Console.Write("Si Desea Continuar, Presione = 1 ------- Sino, Presione = 2: ");
                    respuesta = int.Parse(Console.ReadLine());
                } while (respuesta == 1);
            }
        }
    }
}

